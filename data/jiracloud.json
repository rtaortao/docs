{
  "navigation": {
    "name": "jiracloud",
    "title": "Jira Cloud platform",
    "url": "/cloud/jira/platform/",
    "categories": [
      {
        "title": "Guides",
        "indexLinkTitle": "Latest updates",
        "name": "devguide",
        "url": "/cloud/jira/platform/",
        "subcategories": [
          {
            "title": "Introduction and basics",
            "expandAlways": true,
            "name": "intro",
            "items": [
              {
                "title": "Integrating with Jira Cloud",
                "url": "/cloud/jira/platform/integrating-with-jira-cloud"
              },
              {
                "title": "Getting started",
                "url": "/cloud/jira/platform/getting-started"
              },
              {
                "title": "Extending the user interface",
                "url": "/cloud/jira/platform/extending-the-user-interface"
              },
              {
                "title": "Frameworks and tools",
                "url": "/cloud/jira/platform/frameworks-and-tools"
              },
              {
                "title": "Security overview",
                "url": "/cloud/jira/platform/security-overview"
              }
            ]
          },
          {
            "title": "Security for Connect apps",
            "expandAlways": true,
            "name": "securityconnect",
            "items": [
              {
                "title": "Security for Connect apps",
                "url": "/cloud/jira/platform/security-for-connect-apps"
              },
              {
                "title": "Authentication for Connect apps",
                "url": "/cloud/jira/platform/authentication-for-apps"
              },
              {
                "title": "Understanding JWT for Connect apps",
                "url": "/cloud/jira/platform/understanding-jwt"
              },
              {
                "title": "User impersonation for Connect apps",
                "url": "/cloud/jira/platform/oauth-2-jwt-bearer-token-authorization-grant-type"
              }
            ]
          },
          {
            "title": "Security for other integrations",
            "expandAlways": true,
            "name": "securityother",
            "items": [
              {
                "title": "Security for other integrations",
                "url": "/cloud/jira/platform/security-for-other-integrations"
              },
              {
                "title": "OAuth 2.0 authorization code grants (3LO) for apps",
                "url": "/cloud/jira/platform/oauth-2-authorization-code-grants-3lo-for-apps"
              },
              {
                "title": "OAuth for REST APIs",
                "url": "/cloud/jira/platform/jira-rest-api-oauth-authentication"
              },
              {
                "title": "Basic auth for REST APIs",
                "url": "/cloud/jira/platform/jira-rest-api-basic-authentication"
              },
              {
                "title": "Cookie-based auth for REST APIs",
                "url": "/cloud/jira/platform/jira-rest-api-cookie-based-authentication"
              }
            ]
          },
          {
            "title": "Learning",
            "expandAlways": true,
            "name": "guides",
            "items": [
              {
                "title": "User Privacy App Developer Guide",
                "url": "/cloud/jira/platform/user-privacy-developer-guide"
              },
              {
                "title": "Connect cookbook",
                "url": "/cloud/jira/platform/connect-cookbook"
              },
              {
                "title": "Cacheable app iframes",
                "url": "/cloud/jira/platform/cacheable-app-iframes/"
              },
              {
                "title": "Patterns and examples",
                "url": "/cloud/jira/platform/patterns-and-examples/"
              },
              {
                "id": "tutorialsAndGuides",
                "title": "Tutorials and guides",
                "url": "/cloud/jira/platform/tutorials-and-guides/"
              },
              {
                "title": "Storing data without a database",
                "url": "/cloud/jira/platform/storing-data-without-a-database"
              }
            ]
          },
          {
            "title": "Design guidelines",
            "expandAlways": true,
            "name": "design",
            "items": [
              {
                "title": "Issue detail view",
                "url": "/cloud/jira/platform/issue-view"
              },
              {
                "title": "Use multiple glances in the issue view",
                "url": "/cloud/jira/platform/issue-view-using-multiple-glances"
              }
            ]
          },
          {
            "title": "Building blocks",
            "expandAlways": true,
            "name": "blocks",
            "items": [
              {
                "title": "App descriptor",
                "url": "/cloud/jira/platform/app-descriptor"
              },
              {
                "title": "Conditions",
                "url": "/cloud/jira/platform/conditions"
              },
              {
                "title": "Context parameters",
                "url": "/cloud/jira/platform/context-parameters"
              },
              {
                "title": "Entity properties",
                "url": "/cloud/jira/platform/jira-entity-properties"
              },
              {
                "title": "Internationalization",
                "url": "/cloud/jira/platform/internationalization/"
              },
              {
                "title": "Scopes",
                "url": "/cloud/jira/platform/scopes"
              },
              {
                "title": "Webhooks",
                "url": "/cloud/jira/platform/webhooks"
              }
            ]
          },
          {
            "title": "Other considerations",
            "expandAlways": true,
            "name": "other",
            "items": [
              {
                "title": "Atlassian Design Guidelines",
                "url": "https://design.atlassian.com/"
              },
              {
                "title": "Atlaskit",
                "url": "https://atlaskit.atlassian.com/"
              },
              {
                "title": "Atlassian Marketplace",
                "url": "/platform/marketplace/"
              },
              {
                "title": "Cloud app licensing",
                "url": "/platform/marketplace/cloud-app-licensing/"
              }
            ]
          }
        ]
      },
      {
        "title": "Reference",
        "name": "reference",
        "url": "/cloud/jira/platform/rest",
        "subcategories": [
          {
            "title": "REST API",
            "name": "rest",
            "items": [
              {
                "title": "REST API",
                "url": "/cloud/jira/platform/rest/v3/"
              }
            ]
          },
          {
            "title": "Document Format",
            "name": "document",
            "expandAlways": true,
            "items": [
              {
                "title": "Document structure",
                "url": "/cloud/jira/platform/apis/document/structure"
              },
              {
                "title": "Document builder",
                "url": "/cloud/jira/platform/apis/document/playground"
              },
              {
                "title": "Document viewer",
                "url": "/cloud/jira/platform/apis/document/viewer"
              },
              {
                "title": "Client libraries",
                "url": "/cloud/jira/platform/apis/document/libs"
              },
              {
                "title": "Node - blockquote",
                "url": "/cloud/jira/platform/apis/document/nodes/blockquote"
              },
              {
                "title": "Node - bulletList",
                "url": "/cloud/jira/platform/apis/document/nodes/bulletList"
              },
              {
                "title": "Node - codeBlock",
                "url": "/cloud/jira/platform/apis/document/nodes/codeBlock"
              },
              {
                "title": "Node - doc",
                "url": "/cloud/jira/platform/apis/document/nodes/doc"
              },
              {
                "title": "Node - emoji",
                "url": "/cloud/jira/platform/apis/document/nodes/emoji"
              },
              {
                "title": "Node - hardBreak",
                "url": "/cloud/jira/platform/apis/document/nodes/hardBreak"
              },
              {
                "title": "Node - heading",
                "url": "/cloud/jira/platform/apis/document/nodes/heading"
              },
              {
                "title": "Node - listItem",
                "url": "/cloud/jira/platform/apis/document/nodes/listItem"
              },
              {
                "title": "Node - media",
                "url": "/cloud/jira/platform/apis/document/nodes/media"
              },
              {
                "title": "Node - mediaGroup",
                "url": "/cloud/jira/platform/apis/document/nodes/mediaGroup"
              },
              {
                "title": "Node - mediaSingle",
                "url": "/cloud/jira/platform/apis/document/nodes/mediaSingle"
              },
              {
                "title": "Node - mention",
                "url": "/cloud/jira/platform/apis/document/nodes/mention"
              },
              {
                "title": "Node - orderedList",
                "url": "/cloud/jira/platform/apis/document/nodes/orderedList"
              },
              {
                "title": "Node - panel",
                "url": "/cloud/jira/platform/apis/document/nodes/panel"
              },
              {
                "title": "Node - paragraph",
                "url": "/cloud/jira/platform/apis/document/nodes/paragraph"
              },
              {
                "title": "Node - rule",
                "url": "/cloud/jira/platform/apis/document/nodes/rule"
              },
              {
                "title": "Node - table",
                "url": "/cloud/jira/platform/apis/document/nodes/table"
              },
              {
                "title": "Node - tableCell",
                "url": "/cloud/jira/platform/apis/document/nodes/table_cell"
              },
              {
                "title": "Node - tableHeader",
                "url": "/cloud/jira/platform/apis/document/nodes/table_header"
              },
              {
                "title": "Node - tableRow",
                "url": "/cloud/jira/platform/apis/document/nodes/table_row"
              },
              {
                "title": "Node - text",
                "url": "/cloud/jira/platform/apis/document/nodes/text"
              },
              {
                "title": "Mark - code",
                "url": "/cloud/jira/platform/apis/document/marks/code"
              },
              {
                "title": "Mark - em",
                "url": "/cloud/jira/platform/apis/document/marks/em"
              },
              {
                "title": "Mark - link",
                "url": "/cloud/jira/platform/apis/document/marks/link"
              },
              {
                "title": "Mark - strike",
                "url": "/cloud/jira/platform/apis/document/marks/strike"
              },
              {
                "title": "Mark - strong",
                "url": "/cloud/jira/platform/apis/document/marks/strong"
              },
              {
                "title": "Mark - subsup",
                "url": "/cloud/jira/platform/apis/document/marks/subsup"
              },
              {
                "title": "Mark - textColor",
                "url": "/cloud/jira/platform/apis/document/marks/textColor"
              },
              {
                "title": "Mark - underline",
                "url": "/cloud/jira/platform/apis/document/marks/underline"
              }
            ]
          },
          {
            "title": "REST API v2",
            "name": "rest",
            "items": [
              {
                "title": "REST API v2",
                "url": "/cloud/jira/platform/rest/v2/"
              }
            ]
          },
          {
            "title": "Modules",
            "name": "modules",
            "items": [
              {
                "title": "About Jira Platform modules",
                "url": "/cloud/jira/platform/about-jira-modules"
              },
              {
                "title": "Administration UI locations",
                "url": "/cloud/jira/platform/administration-ui-locations"
              },
              {
                "title": "Dashboard item",
                "url": "#"
              },
              {
                "title": "Dialog",
                "url": "#"
              },
              {
                "title": "Entity property",
                "url": "#"
              },
              {
                "title": "Global permission",
                "url": "#"
              },
              {
                "title": "Home container",
                "url": "/cloud/jira/platform/home-container"
              },
              {
                "title": "Issue Content",
                "url": "#"
              },
              {
                "title": "Issue field",
                "url": "#"
              },
              {
                "title": "Issue Glance",
                "url": "#"
              },
              {
                "title": "Issue view UI locations",
                "url": "/cloud/jira/platform/issue-view-ui-locations"
              },
              {
                "title": "New issue view UI locations",
                "url": "/cloud/jira/platform/new-issue-view-ui-locations"
              },
              {
                "title": "Keyboard shortcut",
                "url": "#"
              },
              {
                "title": "Page",
                "url": "#"
              },
              {
                "title": "Project admin tab panel",
                "url": "#"
              },
              {
                "title": "Project Page",
                "url": "#"
              },
              {
                "title": "Project permission",
                "url": "#"
              },
              {
                "title": "Project settings UI locations",
                "url": "/cloud/jira/platform/project-settings-ui-locations"
              },
              {
                "title": "Project sidebar",
                "url": "/cloud/jira/platform/jira-project-sidebar"
              },
              {
                "title": "Report",
                "url": "#"
              },
              {
                "title": "Search request view",
                "url": "#"
              },
              {
                "title": "Tab panel",
                "url": "#"
              },
              {
                "title": "Time Tracking Provider",
                "url": "#"
              },
              {
                "title": "User profile menu",
                "url": "/cloud/jira/platform/user-profile-menu"
              },
              {
                "title": "Web item",
                "url": "#"
              },
              {
                "title": "Web panel",
                "url": "#"
              },
              {
                "title": "Web section",
                "url": "#"
              },
              {
                "title": "Webhook",
                "url": "#"
              },
              {
                "title": "Workflow post function",
                "url": "#"
              }
            ]
          },
          {
            "title": "JavaScript API",
            "name": "jsapi",
            "items": [
              {
                "title": "About the JavaScript API",
                "url": "/cloud/jira/platform/about-the-javascript-api"
              }
            ]
          },
          {
            "title": "App properties API",
            "name": "appapi",
            "items": [
              {
                "title": "App properties API",
                "url": "/cloud/jira/platform/app-properties-api/"
              }
            ]
          }
        ]
      },
      {
        "name": "help",
        "title": "Resources",
        "url": "/cloud/jira/platform/get-help/"
      }
    ]
  }
}
