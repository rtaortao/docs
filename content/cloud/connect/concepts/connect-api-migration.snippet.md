# API migration Opt-in


## Introduction

Latest release of Atlassian Connect has introduced API Migrations, a new mechanism for apps to selectively enrol into a new connect behavior.
With this capability, apps are now able to specify that they are compatible with new  features of Atlassian Connect during its introduction period.

The key feature of this functionality is to allow app developers to work on the changes at their own time and signal to Connect when they are ready to make the switch.
Development of this mechanism was motivated by introduction of API changes that could not be made backward compatible. Such changes will *break* existing apps that do not yet support new API version. The opt-in mechanism allows vendors *some* degree of control over the data received from Atlassian Connect, based on which APIs they opted in to.
For example, the GDPR API migration allows vendors to specify when an app is ready to accept new data that includes user account ID and no longer rely on personal data such as email address being passed. This way apps don't need to wait until the end of deprecation period when the old fields will be removed for everyone as they can request new behavior when apps are ready. It also useful for development on new app versions.

## How to opt in

Connect app can subscribe to a new migration via newly introduced optional top-level field called `apiMigrations` in the [app-descriptor](../app-descriptor/). To opt-in to active migrations, an app needs to specify migration key (for example, `gdpr` for GDPR migration) and a migration value (dependent on the specific Migration). The value for API migrations can be a boolean flag (opted in/out) or in some cases an enumeration to specify one of all possible states. For example, in order to opt in to GDPR, add the following to your app's descriptor:

```JSON
{
    "key": "my-app",
    "name": "My App",
    ...,
    "apiMigrations": {
        "gdpr": true
    }
}
```

The app with the above in it's app-descriptor will be considered enrolled into GDPR API migration. Once this new descriptor is picked up by automatic Connect app update, the app will no longer sent personal data in responses, payloads and web-hook notifications.

Please note that API migration subscription is only relevant during the period when migration is *active*. If apps specify an API Migration in their descriptor, the provided value will be used to determine their enrolment status, otherwise they are subject to default system behavior. After the migration period has ended, the default value of the API Migration will change and thus enforcing new behavior on all apps, regardless of the value specified in the descriptor. For example, please consider GDPR API migration:

 - **Before the migration period**: the value of API subscription in the app descriptor is ignored.
 - **During the migration period**: app can specify whether they are ready or not to use new APIs by setting `"gdpr": true` in the descriptor.
 - **After the migration period**: all the apps will no longer receive personal data regardless of the value of the `gdpr` field.

## Active API migrations

You can specify migrations that you would like to opt-in using `apiMigrations` property.

See [active migrations](../connect-active-api-migrations/) for the list of currently active API migrations.
