---
title: "Custom events"
platform: cloud
product: bitbucketcloud
category: reference
subcategory: modules
date: "2018-07-20"
---
# Custom Events

The Custom Events module enables applications to publish their own events
via the Bitbucket Cloud site, so that other applications can subscribe
to these events as if they were a regular webhook event.

## Sample descriptor JSON

Declaring a custom event in a Connect [app descriptor](/cloud/bitbucket/app-descriptor) looks like this:

``` json
{
    "key": "com.example.app",
    "definitions": {
        "com.example.app:myobject": {
            "type": "object",
            "description": "An object",
            "properties": {
                "myproperty": {
                    "type": "string",
                    "description": "A property"
                }
            }
        }
    },
    "modules": {
        "customEvents": {
            "com.example.app:myevent": {
                "schema": {
                    "properties": {
                        "obj": {
                            "$ref": "#/definitions/com.example.app:myobject"
                        },
                        "repository": {
                            "$ref": "#/definitions/repository"
                        }
                    }
                },
                "fields": ["repository.creator"]
            }
        }
    }
}
```

Declare [definitions](/cloud/bitbucket/app-descriptor/#definitions)
in a separate top-level section of the descriptor.
This separates the declaration of application-specific object types
from the events that reference them. In the above example, the application
owns a custom event called <code>com.example.app:myevent</code>,
the schema of which references the <code>com.example.app:myobject</code>
definition, as well as the built-in <code>repository</code> type.

## Event names <a name="event-names"></a>

Each custom event name must be prefixed with the key of the application,
otherwise validation of the descriptor will fail.

### Properties <a name="properties"></a>

<table>
    <thead>
  <tr>
    <th colspan="3"></th>
  </tr>
  </thead>
  <tbody>
      <!-- Schema -->
      <tr style="border-collapse: collapse; border: none;">
        <td colspan="3"><h3><code>schema</code></h3></td>
      </tr>
      <tr style="border-collapse: collapse; border: none;">
          <td style="column-width: 2px"> </td>
          <td valign="top"><b>Type</b></td>
          <td>object</td>
      </tr>
      <tr style="border-collapse: collapse; border: none;">
        <td></td>
        <td><b>Required</b></td>
        <td><span class="aui-lozenge aui-lozenge-error">Yes</span></td>
      </tr>
      <tr style="border-collapse: collapse; border: none;">
        <td></td>
        <td valign="top"><b>Description</b></td>
        <td>
        A <a href="http://json-schema.org">JSON Schema</a> object that
        describes the shape of the custom event
        object, which may reference application-specific object types or
        objects native to Bitbucket Cloud, like <code>repository</code>.
        </td>
      </tr>
      <!-- Fields -->
      <tr style="border-collapse: collapse; border: none;">
        <td valign="top" rowspan="3"><code>fields</code></td>
        <td valign="top"><b>Type</b></td>
        <td>array</td>
      </tr>
      <tr style="border-collapse: collapse; border: none;">
        <td valign="top"><b>Required</b></td>
        <td valign="top"><span class="aui-lozenge">No</span></td>
      </tr>
      <tr style="border-collapse: collapse; border: none;">
        <td valign="top"><b>Description</b></td>
        <td colspan="2">An optional array of additional fields to include
        in the custom event object. For example,
        if a Bitbucket Cloud repository object is included at the top level of
        a custom event object, the <code>fields</code> property can be set to
        <code>['repository.creator']</code> so that,
        in addition to the default repository fields, the repository creator
        is also included in the event sent to subscribers.
        </td>
      </tr>
  </tbody>
</table>
