---
title: "Cookie"
platform: cloud
product: bitbucketcloud
category: reference
subcategory: javascript 
date: "2017-05-18"
---

# Cookie

Allows apps to store, retrieve, and erase cookies against the host Jira/Confluence. These cannot be seen by other apps.

## Methods
            
### erase (name)
        
Remove the given cookie.
    
#### Parameters  

<table>
    <thead>
	<tr>
		<th>Name</th>

		<th>Type</th>

		<th>Description</th>
	</tr>
	</thead>
	<tbody>
        <tr>
            <td><code>name</code></td>

            <td>String</td>
            <td>the name of the cookie to remove</td>
        </tr>
	</tbody>
</table>

#### Example
        
``` javascript
AP.require("cookie", function(cookie){
  cookie.save('my_cookie', 'my value', 1);
  cookie.read('my_cookie', function(value) { alert(value); });
  cookie.erase('my_cookie');
});
```

### read (name, callback)

Get the value of a cookie.
    
#### Parameters  

<table>
    <thead>
	<tr>

		<th>Name</th>
		<th>Type</th>

		<th>Description</th>
	</tr>
	</thead>
	<tbody>
        <tr>
                <td><code>name</code></td>
            <td>String</td>
            <td>name of cookie to read</td>
        </tr>
        <tr>
                <td><code>callback</code></td>
            <td>function</td>

            <td>callback to pass cookie data</td>
        </tr>
	</tbody>
</table>

#### Example
        
``` javascript
AP.require("cookie", function(cookie){
  cookie.save('my_cookie', 'my value', 1);
  cookie.read('my_cookie', function(value) { alert(value); });
});
```
### save (name, value, expires)
    
Save a cookie.
    
#### Parameters

<table>
    <thead>
	<tr>
		<th>Name</th>
		<th>Type</th>

		<th>Description</th>
	</tr>
	</thead>

	<tbody>
        <tr>
                <td><code>name</code></td>
            <td>String</td>
            <td>name of cookie</td>
        </tr>

        <tr>
                <td><code>value</code></td>
            <td>String</td>

            <td>value of cookie</td>
        </tr>
        <tr>
                <td><code>expires</code></td>
            <td>Number</td>

            <td>number of days before cookie expires</td>
        </tr>
	
	</tbody>
</table>

#### Example
        
``` javascript
AP.require("cookie", function(cookie){
  cookie.save('my_cookie', 'my value', 1);
});
```