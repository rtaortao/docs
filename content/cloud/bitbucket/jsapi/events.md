---
title: "Events"
platform: cloud
product: bitbucketcloud
category: reference
subcategory: javascript 
date: "2017-05-18"
---

# Events

The Events module provides a mechanism for emitting and receiving events.

## Basic example

``` javascript
//The following will create an alert message every time the event `customEvent` is triggered.
AP.require('events', function(events){
  events.on('customEvent', function(){
      alert('event fired');
  });
  events.emit('customEvent');
});
```

## Methods

### emit (name, args)
    
Emits an event on this bus, firing listeners by name as well as all `any` listeners. Arguments following the name parameter are captured and passed to listeners.

#### Parameters

<table>
    <thead>
	<tr>
		<th>Name</th>
		<th>Type</th>
		<th>Description</th>
	</tr>
	</thead>
	<tbody>
        <tr>
            <td><code>name</code></td>
            <td>String</td>
            <td>The name of event to emit</td>
        </tr>
        <tr>
                <td><code>args</code></td>
            <td>Array.&lt;String></td>
            <td>0 or more additional data arguments to deliver with the event</td>
        </tr>
	</tbody>
</table>
   

### off (name, listener)

Removes a particular listener for an event.
    
#### Parameters

<table>
    <thead>
	<tr>
		<th>Name</th>
		<th>Type</th>
		<th>Description</th>
	</tr>
	</thead>

	<tbody>
        <tr>
                <td><code>name</code></td>
            <td>String</td>
            <td>The event name to unsubscribe the listener from</td>
        </tr>
        <tr>
        <td><code>listener</code></td>
            <td>function</td>
            <td>The listener callback to unsubscribe from the event name</td>
        </tr>
	</tbody>
</table>
   

### offAll name

Removes all listeners from an event name, or unsubscribes all event-name-specific listeners
if no name if given.

#### Parameters

<table>
    <thead>
	<tr>
		<th>Name</th>
		<th>Type</th>
		<th>Argument</th>
		<th>Description</th>
	</tr>
	</thead>
	<tbody>
        <tr>
                <td><code>name</code></td>
            <td>String</td>
            
                <td>&lt;optional></td>

            <td>The event name to unsubscribe all listeners from</td>
        </tr>
	
	</tbody>
</table>
   

### offAny (listener)

Removes an `any` event listener.

#### Parameters

<table>
    <thead>
	<tr>
		<th>Name</th>

		<th>Type</th>

		<th>Description</th>
	</tr>
	</thead>

	<tbody>
        <tr>
                <td><code>listener</code></td>
            <td>function</td>
            <td>A listener callback to unsubscribe from any event name</td>
        </tr>
	</tbody>
</table>

   
### on (name, listener)

Adds a listener for all occurrences of an event of a particular name. Listener arguments include any arguments passed to `events.emit`, followed by an object describing the complete event information.

#### Parameters

<table>
    <thead>
	<tr>
		<th>Name</th>
		<th>Type</th>

		<th>Description</th>
	</tr>
	</thead>
	<tbody>
        <tr>
                <td><code>name</code></td>
            <td>String</td>
            <td><p>The event name to subscribe the listener to</p></td>
        </tr>

        <tr>
        <td><code>listener</code></td>
            <td>function</td>
            <td>A listener callback to subscribe to the event name</td>
        </tr>
	</tbody>
</table>
   

### onAny (listener)

Adds a listener for all occurrences of any event, regardless of name. Listener arguments begin with the event name, followed by any arguments passed to `events.emit`, followed by an object describing the complete event information.

#### Parameters
<table>
    <thead>
	<tr>
		<th>Name</th>
		<th>Type</th>
		<th>Description</th>
	</tr>
	</thead>
	<tbody>
        <tr>
                <td><code>listener</code></td>
            <td>function</td>
            <td>A listener callback to subscribe for any event name</td>
        </tr>
	
	</tbody>
</table>
   
   
### once (name, listener)
    
Adds a listener for one occurrence of an event of a particular name. Listener arguments include any argument passed to `events.emit`, followed by an object describing the complete event information.

#### Parameters

<table>
    <thead>
	<tr>
		<th>Name</th>
		<th>Type</th>
		<th>Description</th>
	</tr>
	</thead>
	<tbody>

        <tr>
                <td><code>name</code></td>

            <td>String</td>

            <td>The event name to subscribe the listener to</td>
        </tr>
        <tr>
        <td><code>listener</code></td>
            <td>function</td>

            <td>A listener callback to subscribe to the event name</td>
        </tr>

	</tbody>
</table>
