---
title: "Inline"
platform: cloud
product: bitbucketcloud
category: reference
subcategory: javascript 
date: "2017-05-18"
---

# Inline dialog

The inline dialog is a wrapper for secondary content/controls to be displayed on user request. Consider this 
component as displayed in context to the triggering control with the dialog overlaying the page content. A 
inline dialog should be preferred over a modal dialog when a connection between the action has a clear benefit 
versus having a lower user focus.

For more information, read about the Atlassian User Interface [inline dialog component](https://docs.atlassian.com/aui/latest/).

## Methods

### hide ()

Hide the inline dialog that contains your connect app.

#### Example
        
``` javascript
AP.require('inline-dialog', function(inlineDialog){
  inlineDialog.hide();
});
```
