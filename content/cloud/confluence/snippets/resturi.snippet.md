

URIs for a Confluence REST API resource have the following structure:  

``` javascript
https://your-domain.atlassian.net/wiki/rest/api-name/api-version/resource-name
```

**Examples:**

``` javascript
https://your-domain.atlassian.net/wiki/rest/prototype/1/space/ds
https://your-domain.atlassian.net/wiki/rest/prototype/latest/space/ds
```

