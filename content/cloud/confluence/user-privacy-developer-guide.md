---
title: "User Privacy Developer Guide"
platform: cloud
product: confcloud
category: devguide
subcategory: learning
date: "2018-10-18"
---

{{< include path="docs/content/cloud/connect/guides/user-privacy-developer-guide.snippet.md" >}}
