---
title: "OAuth 2.0 authorization code grants (3LO) for apps (deprecated)"
platform: cloud
product: jiracloud
category: devguide
subcategory: securityother
aliases:
- /jiracloud/oauth-2-authorization-code-grants-3lo-for-apps.html
- /jiracloud/oauth-2-authorization-code-grants-3lo-for-apps.md
date: "2018-08-10"
---
# DEPRECATED PAGE: OAuth 2.0 authorization code grants (3LO) for apps

{{% warning title="Deprecated"%}}
<p>This page has been deprecated in favour of <a href="/cloud/jira/platform/oauth-2-authorization-code-grants-3lo-for-apps">OAuth 2.0 authorization code grants (3LO) for apps</a> and will be deleted soon. Please do not update this page or use the instructions on this page.</p>{{% /warning %}}

