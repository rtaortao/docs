---
title: "User impersonation for Connect apps"
platform: cloud
product: jiracloud
category: devguide
subcategory: securityconnect
date: "2018-06-15"
---
{{< include path="docs/content/cloud/connect/concepts/oauth2-jwt-bearer-token-authentication.snippet.md">}}
