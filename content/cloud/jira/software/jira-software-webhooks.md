---
title: "Jira Software Webhooks" 
platform: cloud
product: jswcloud
category: devguide
subcategory: blocks
aliases:
- /jiracloud/jira-software-webhooks-39990316.html
- /jiracloud/jira-software-webhooks-39990316.md
confluence_id: 39990316
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=39990316
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=39990316
date: "2017-09-11"
---
# Jira Software webhooks

Jira Software webhooks are simply Jira webhooks that are registered for Jira Software events. Jira Software webhooks are configured in the same way that you configure Jira platform webhooks. You can register them via the Jira administration console, you can add them as a post function to a workflow, you can inject variables into the webhook URL, etc. 

To learn more, read the Jira platform documentation on [webhooks].

  [webhooks]: /cloud/jira/platform/webhooks
