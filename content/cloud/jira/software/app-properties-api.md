---
title: "App properties API"
platform: cloud
product: jswcloud
category: reference
subcategory: appapi
date: "2018-06-13"
---

{{< reuse-page path="docs/content/cloud/jira/platform/app-properties-api.md">}}
