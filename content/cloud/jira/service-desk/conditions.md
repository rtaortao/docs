---
title: Conditions
platform: cloud
product: jsdcloud
category: devguide
subcategory: blocks
aliases:
    - /jiracloud/conditions.html
    - /jiracloud/conditions.md
    - /cloud/jira/service-desk/conditions.html
    - /cloud/jira/service-desk/conditions.html
date: "2017-08-24"
---
{{< include path="docs/content/cloud/connect/concepts/jira-conditions.snippet.md">}}