---
title: Understanding JWT
platform: cloud
product: jsdcloud
category: devguide
subcategory: security
date: "2017-08-25"
---
{{< include path="docs/content/cloud/connect/concepts/understanding-jwt.snippet.md" >}}