---
title: "Customer portal" 
platform: cloud
product: jsdcloud
category: reference
subcategory: modules
aliases:
    - /jiracloud/jira-service-desk-modules-customer-portal-39988271.html
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=39988271
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=39988271
confluence_id: 39988271
date: "2017-09-11"
---
# Customer portal

This pages lists the Jira Service Desk modules for the customer portal. These can be used to inject panels and actions to various locations in Jira Service Desk customer portal.

A panel is simply a section of HTML content on the page. An action is a clickable link or menu entry for user to perform specific operation. The target location of this link or menu entry is defined in the `atlassian-connect.json` descriptor file. The target location can be a URL hosted in the host application (Jira Service Desk) or your Atlassian Connect app.

## Header panel

A panel rendered at the top of customer portal pages.

#### Module type
`serviceDeskPortalHeader`

#### Screenshot
<img src="../images/jdev-service-desk-portal-header.png"/> 

#### Sample JSON
``` json
...
 "modules": {
     "serviceDeskPortalHeaders": [
         {
             "key": "sd-portal-header",
             "url": "/sd-portal-header"
         }
     ]
 }
...
```

#### Properties

`key`

-   **Type**: `string (^[a-zA-Z0-9-]+$)`
-   **Required**: yes
-   **Description**: A key to identify this module. This key must be unique relative to the app, with the exception of Confluence macros: their keys need to be globally unique. Keys must only contain alphanumeric characters and dashes.

`url`

-   **Type**: `string`, `uri-template`
-   **Required**: yes
-   **Description**: The URL of the app resource that provides the content. This URL must be relative to the app's base URL. Your app can receive [additional context] from the application by using variable tokens in the URL attribute.

`weight`

-   **Type**: `integer`
-   **Default**: 100
-   **Description**: Determines the order in which the web item appears in the menu or list. The "lightest" weight (i.e., lowest number) appears first, rising relative to other items, while the "heaviest" weights sink to the bottom of the menu or list.<br>Built-in web items have weights that are incremented by numbers that leave room for additional items, such as by 10 or 100. Be mindful of the weight you choose for your item, so that it appears in a sensible order given existing items.

`conditions`

-   **Type**: [ [single condition], [composite condition], ... ]
-   **Description**: [Conditions] can be added to display only when all the given conditions are true.

`pages`

-   **Type**: `[ string, ... ]`
-   **Allowed values**: `help_center`, `portal`, `create_request`, `view_request`, `my_requests`, `approvals`, `profile`
-   **Description**: Restrict the module to only be visible in specified customer portal page(s).

----

## Sub-header panel

A panel rendered underneath the title of customer portal pages.

#### Module type
`serviceDeskPortalSubHeader`

#### Screenshot
<img src="../images/jdev-service-desk-portal-sub-header.png"/> 

#### Sample JSON
``` json
...
"modules": {
    "serviceDeskPortalSubHeaders": [
            {
            "key": "sd-portal-subheader",
            "url": "/sd-portal-subheader"
            }
        ]
    }
...
```

#### Properties

`key`

-   **Type**: `string (^[a-zA-Z0-9-]+$)`
-   **Required**: yes
-   **Description**: A key to identify this module. This key must be unique relative to the app, with the exception of Confluence macros: their keys need to be globally unique. Keys must only contain alphanumeric characters and dashes.

`url`

-   **Type**: `string`, `uri-template`
-   **Required**: yes
-   **Description**:  The URL of the app resource that provides the content. This URL must be relative to the app's base URL. Your app can receive [additional context] from the application by using variable tokens in the URL attribute.

`weight`

-   **Type**: `integer`
-   **Default**: 100
-   **Description**: Determines the order in which the web item appears in the menu or list. The "lightest" weight (i.e., lowest number) appears first, rising relative to other items, while the "heaviest" weights sink to the bottom of the menu or list.<br>Built-in web items have weights that are incremented by numbers that leave room for additional items, such as by 10 or 100. Be mindful of the weight you choose for your item, so that it appears in a sensible order given existing items.

`conditions`

-   **Type**: [ [single condition], [composite condition], ... ]
-   **Description**: [Conditions] can be added to display only when all the given conditions are true.

`pages`

-   **Type**: `[ string, ... ]`
-   **Allowed values**: help_center, portal, create_request, view_request, my_requests, approvals, profile
-   **Description**: Restrict the module to only be visible in specified customer portal page(s).

----

## Footer panel

A panel rendered at the bottom of customer portal pages.

#### Module type
`serviceDeskPortalFooter`

#### Screenshot
<img src="../images/jdev-service-desk-portal-footer.png"/> 

#### Sample JSON
``` json
...
"modules": {
    "serviceDeskPortalFooters": [
        {
            "key": "sd-portal-footer",
            "url": "/sd-portal-footer"
        }
    ]
}
...
```

#### Properties

`key`

-   **Type**: `string (^[a-zA-Z0-9-]+$)`
-   **Required**: yes
-   **Description**: A key to identify this module. This key must be unique relative to the app, with the exception of Confluence macros: their keys need to be globally unique. Keys must only contain alphanumeric characters and dashes.

`url`

-   **Type**: `string`, `uri-template`
-   **Required**: yes
-   **Description**:  The URL of the app resource that provides the content. This URL must be relative to the app's `baseUrl`. Your app can receive [additional context] from the application by using variable tokens in the URL attribute.

`weight`

-   **Type**: `integer`
-   **Default**: 100
-   **Description**: Determines the order in which the web item appears in the menu or list. The "lightest" weight (i.e., lowest number) appears first, rising relative to other items, while the "heaviest" weights sink to the bottom of the menu or list.<br>Built-in web items have weights that are incremented by numbers that leave room for additional items, such as by 10 or 100. Be mindful of the weight you choose for your item, so that it appears in a sensible order given existing items.

`conditions`

-   **Type**: [ [single condition], [composite condition], ... ]
-   **Description**: [Conditions] can be added to display only when all the given conditions are true.

`pages`

-   **Type**: `[ string, ... ]`
-   **Allowed values**: `help_center`, `portal`, `create_request`, `view_request`, `my_requests`, `approvals`, `profile`
-   **Description**: Restrict the module to only be visible in specified customer portal page(s).

----

## Request view panel

A panel that shows up in the bottom-right corner of request view page.

#### Module type
`serviceDeskPortalRequestViewPanel`

#### Screenshot
<img src="../images/jdev-service-desk-portal-request-view-panel.png"/> 

#### Sample JSON
``` json
...
"modules": {
    "serviceDeskPortalRequestViewPanels": [
        {
            "key": "sd-portal-request-view-content",
            "url": "/sd-portal-request-view-content"
        }
    ]
}
...
```

#### Properties

`key`

-   **Type**: `string (^[a-zA-Z0-9-]+$)`
-   **Required**: yes
-   **Description**: A key to identify this module. This key must be unique relative to the app, with the exception of Confluence macros: their keys need to be globally unique. Keys must only contain alphanumeric characters and dashes.

`url`

-   **Type**: `string`, `uri-template`
-   **Required**: yes
-   **Description**:  The URL of the app resource that provides the content. This URL must be relative to the app's base URL. Your app can receive [additional context] from the application by using variable tokens in the URL attribute.

`weight`

-   **Type**: `integer`
-   **Default**: 100
-   **Description**: Determines the order in which the web item appears in the menu or list. The "lightest" weight (i.e., lowest number) appears first, rising relative to other items, while the "heaviest" weights sink to the bottom of the menu or list.<br>Built-in web items have weights that are incremented by numbers that leave room for additional items, such as by 10 or 100. Be mindful of the weight you choose for your item, so that it appears in a sensible order given existing items.

`conditions`

-   **Type**: [ [single condition], [composite condition], ... ]
-   **Description**: [Conditions] can be added to display only when all the given conditions are true.

----

## Request view details panel

A panel that shows up in the "details" section of a request in the request view page.

#### Module type
`serviceDeskPortalRequestViewDetailsPanel`

#### Screenshot
<img src="../images/jdev-service-desk-portal-request-view-details-panel.png"/> 

#### Sample JSON
``` json
...
"modules": {
    "serviceDeskPortalRequestViewDetailsPanels": [
        {
            "key": "sd-portal-request-view-details-content",
            "url": "/sd-portal-request-view-details-content"
        }
    ]
}
...
```

#### Properties

`key`

-   **Type**: `string (^[a-zA-Z0-9-]+$)`
-   **Required**: yes
-   **Description**: A key to identify this module. This key must be unique relative to the app, with the exception of Confluence macros: their keys need to be globally unique. Keys must only contain alphanumeric characters and dashes.

`url`

-   **Type**: `string`, `uri-template`
-   **Required**: yes
-   **Description**:  The URL of the app resource that provides the content. This URL must be relative to the app's base URL. Your app can receive [additional context] from the application by using variable tokens in the URL attribute.

`weight`

-   **Type**: `integer`
-   **Default**: 100
-   **Description**: Determines the order in which the web item appears in the menu or list. The "lightest" weight (i.e., lowest number) appears first, rising relative to other items, while the "heaviest" weights sink to the bottom of the menu or list.<br>Built-in web items have weights that are incremented by numbers that leave room for additional items, such as by 10 or 100. Be mindful of the weight you choose for your item, so that it appears in a sensible order given existing items.

`conditions`

-   **Type**: [ [single condition], [composite condition], ... ]
-   **Description**: [Conditions] can be added to display only when all the given conditions are true.

----


## Profile page panel

A panel that shows up in the profile page of the customer portal.

#### Module type
`serviceDeskPortalProfilePanel`

#### Screenshot
<img src="../images/jdev-service-desk-portal-profile-panel.png"/> 

#### Sample JSON
``` json
...
"modules": {
    "serviceDeskPortalProfilePanels": [
        {
            "key": "sd-portal-profile-content",
            "url": "/sd-portal-profile-content"
        }
    ]
}
...
```

#### Properties

`key`

-   **Type**: `string (^[a-zA-Z0-9-]+$)`
-   **Required**: yes
-   **Description**: A key to identify this module. This key must be unique relative to the app, with the exception of Confluence macros: their keys need to be globally unique. Keys must only contain alphanumeric characters and dashes.

`url`

-   **Type**: `string`, `uri-template`
-   **Required**: yes
-   **Description**:  The URL of the app resource that provides the content. This URL must be relative to the app's base URL. Your app can receive [additional context] from the application by using variable tokens in the URL attribute.

`weight`

-   **Type**: `integer`
-   **Default**: 100
-   **Description**: Determines the order in which the web item appears in the menu or list. The "lightest" weight (i.e., lowest number) appears first, rising relative to other items, while the "heaviest" weights sink to the bottom of the menu or list.<br>Built-in web items have weights that are incremented by numbers that leave room for additional items, such as by 10 or 100. Be mindful of the weight you choose for your item, so that it appears in a sensible order given existing items.

`conditions`

-   **Type**: [ [single condition], [composite condition], ... ]
-   **Description**: [Conditions] can be added to display only when all the given conditions are true.

----

## Request view action

A link that shows up in the request view of the customer portal.

#### Module type
`serviceDeskPortalRequestViewAction`

#### Screenshot
<img src="../images/jdev-service-desk-portal-request-view-action.png"/> 

#### Sample JSON
``` json
...
"modules": {
    "serviceDeskPortalRequestViewActions": [
        {
            "key": "sd-portal-request-view-action",
            "name": {
                "value": "My request view action"
            },
            "url": "/sd-portal-request-view-content",
            "icon": {
              "url": "/img/testicon.png"
            }
        }
    ]
}
...
```

#### Properties

`key`

-   **Type**: `string (^[a-zA-Z0-9-]+$)`
-   **Required**: yes
-   **Description**: A key to identify this module. This key must be unique relative to the app, with the exception of Confluence macros: their keys need to be globally unique. Keys must only contain alphanumeric characters and dashes.

`name`

-   **Type**: [i18n Property]
-   **Required**: yes
-   **Description**: A human readable name.

`context`

-   **Type**: `string`
-   **Allowed values**: `page`, `addon`, `product`
-   **Default**: addon
-   **Description**: The context for the URL parameter, if the URL is specified as a relative (not absolute) URL. This context can be on of the following: `addon` which renders the URL relative to the app's base URL, `page` which targets a page module by specifying the page's module key as the URL, or `product` which renders the URL relative to the product's `baseUrl`.

`url`

-   **Type**: `string`, `uri-template`
-   **Required**: yes
-   **Description**:  The URL of the app resource that provides the content. This URL must be relative to the app's base URL. Your app can receive [additional context] from the application by using variable tokens in the URL attribute.

`target`

-   **Type**: [web item target]
-   **Description**:  Defines the way the URL is opened in the browser, such as in its own page or a modal dialog. If omitted, the URL behaves as a regular hyperlink.

`icon`

-   **Type**: [portal icon]
-   **Description**:  An icon that will be rendered next to the action. If the URL is relative it will be resolved against app's base URL. If none provided a default one will be rendered. Icon should follow this guideline: monochromatic with `#333333` color and transparent background, 16 x 16 pixel in size.

`weight`

-   **Type**: `integer`
-   **Default**: 100
-   **Description**: Determines the order in which the web item appears in the menu or list. The "lightest" weight (i.e., lowest number) appears first, rising relative to other items, while the "heaviest" weights sink to the bottom of the menu or list.<br>Built-in web items have weights that are incremented by numbers that leave room for additional items, such as by 10 or 100. Be mindful of the weight you choose for your item, so that it appears in a sensible order given existing items.

`conditions`

-   **Type**: [ [single condition], [composite condition], ... ]
-   **Description**: [Conditions] can be added to display only when all the given conditions are true.

----

## Profile page action

A link that shows up in the profile page.

#### Module type
`serviceDeskPortalProfileAction`

#### Screenshot
<img src="../images/jdev-service-desk-portal-profile-action.png"/> 

#### Sample JSON
``` json
...
"modules": {
    "serviceDeskPortalProfileActions": [
        {
            "key": "sd-portal-profile-action",
            "name": {
                "value": "My profile page action"
            },
            "url": "/sd-portal-profile-content"
        }
    ]
}
...
```

#### Properties

`key`

-   **Type**: `string (^[a-zA-Z0-9-]+$)`
-   **Required**: yes
-   **Description**: A key to identify this module. This key must be unique relative to the app, with the exception of Confluence macros: their keys need to be globally unique. Keys must only contain alphanumeric characters and dashes.

`name`

-   **Type**: [i18n Property]
-   **Required**: yes
-   **Description**: A human readable name.

`context`

-   **Type**: `string`
-   **Allowed values**: page, PAGE, addon, ADDON, product, PRODUCT
-   **Default**: addon
-   **Description**: The context for the URL parameter, if the URL is specified as a relative (not absolute) URL. This context can be on of the following: 'addon' which renders the URL relative to the app's base URL, 'page' which targets a page module by specifying the page's module key as the URL, or 'product' which renders the URL relative to the product's base URL.

`url`

-   **Type**: `string`, `uri-template`
-   **Required**: yes
-   **Description**:  The URL of the app resource that provides the content. This URL must be relative to the app's base URL. Your app can receive [additional context] from the application by using variable tokens in the URL attribute.

`target`

-   **Type**: [web item target]
-   **Description**:  Defines the way the URL is opened in the browser, such as in its own page or a modal dialog. If omitted, the URL behaves as a regular hyperlink.

`weight`

-   **Type**: `integer`
-   **Default**: 100
-   **Description**: Determines the order in which the web item appears in the menu or list. The "lightest" weight (i.e., lowest number) appears first, rising relative to other items, while the "heaviest" weights sink to the bottom of the menu or list.<br>Built-in web items have weights that are incremented by numbers that leave room for additional items, such as by 10 or 100. Be mindful of the weight you choose for your item, so that it appears in a sensible order given existing items.

`conditions`

-   **Type**: [ [single condition], [composite condition], ... ]
-   **Description**: [Conditions] can be added to display only when all the given conditions are true.

----

## User menu action

A menu entry that shows up in the user menu.

#### Module type
`serviceDeskPortalUserMenuAction`

#### Screenshot
<img src="../images/jdev-service-desk-portal-user-menu-action.png"/> 

#### Sample JSON
``` json
...
"modules": {
    "serviceDeskPortalUserMenuActions": [
        {
            "key": "sd-portal-user-menu-action",
            "name": {
                "value": "My user menu action"
            },
            "url": "/sd-portal-user-menu-content"
        }
    ]
}
...
```

#### Properties

`key`

-   **Type**: `string (^[a-zA-Z0-9-]+$)`
-   **Required**: yes
-   **Description**: A key to identify this module. This key must be unique relative to the app, with the exception of Confluence macros: their keys need to be globally unique. Keys must only contain alphanumeric characters and dashes.

`name`

-   **Type**: [i18n Property]
-   **Required**: yes
-   **Description**: A human readable name.

`context`

-   **Type**: `string`
-   **Allowed values**: page, PAGE, addon, ADDON, product, PRODUCT
-   **Default**: addon
-   **Description**: The context for the URL parameter, if the URL is specified as a relative (not absolute) URL. This context can be on of the following: 'addon' which renders the URL relative to the app's base URL, 'page' which targets a page module by specifying the page's module key as the URL, or 'product' which renders the URL relative to the product's base URL.

`url`

-   **Type**: `string`, `uri-template`
-   **Required**: yes
-   **Description**:  The URL of the app resource that provides the content. This URL must be relative to the app's base URL. Your app can receive [additional context] from the application by using variable tokens in the URL attribute.

`target`

-   **Type**: [web item target]
-   **Description**:  Defines the way the URL is opened in the browser, such as in its own page or a modal dialog. If omitted, the URL behaves as a regular hyperlink.

`weight`

-   **Type**: `integer`
-   **Default**: 100
-   **Description**: Determines the order in which the web item appears in the menu or list. The "lightest" weight (i.e., lowest number) appears first, rising relative to other items, while the "heaviest" weights sink to the bottom of the menu or list.<br>Built-in web items have weights that are incremented by numbers that leave room for additional items, such as by 10 or 100. Be mindful of the weight you choose for your item, so that it appears in a sensible order given existing items.

`conditions`

-   **Type**: [ [single condition], [composite condition], ... ]
-   **Description**: [Conditions] can be added to display only when all the given conditions are true.


  [additional context]: /cloud/jira/service-desk/context-parameters
  [single condition]: /cloud/jira/platform/modules/single-condition
  [composite condition]: /cloud/jira/platform/modules/composite-condition
  [Conditions]: /cloud/jira/service-desk/conditions
  [i18n property]: /cloud/jira/platform/modules/i18n-property
  [web item target]: /cloud/jira/platform/modules/web-item-target
  [portal icon]: /cloud/jira/service-desk/portal-icon

