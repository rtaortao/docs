<h2>Why build with Atlassian?</h2>

Whether you're customizing your instance or integrating your app to reach our millions of users, the Atlassian platform provides the tools and flexibility to do it. <a href="/why-build-with-atlassian/">Learn more <i class="fa fa-arrow-right" aria-hidden="true"> </i></a>
